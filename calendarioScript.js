//traz para a pagina as informacoes de token e nome
let codeAutorization = localStorage.getItem("wrCodeAutorization");
let nomeUsuarioLogin = localStorage.getItem("wrNomeUsuarioLogin");
let ano = "2018";
let mes = "04"; 
let feriadosAno;
let qtdeFeriadosAno;
let atividades = "";

document.getElementById("calendarioNome").innerHTML = "Olá " + nomeUsuarioLogin;

// chama API de feriados
function getFeriadosAno(){
    
    fetch("https://api.calendario.com.br/?json=true&ano=2018&ibge=3550308&token=bW9uaWNhLm1hcnRpbmVzQHVvbC5jb20uYnImaGFzaD0yMjg4OTE1MDk=" , {
    method: "GET",
    headers: {
        'Content-type': 'application/json'
    }
}).then(resposta => {
    if(resposta.status !== 200){
        resp.innerHTML = "Erro inesperado. Resposta status: " + resposta.status;
        return;
    } else{
        return resposta.json();
    }
}).then(dados => {
    feriadosAno = dados;
    qtdeFeriadosAno = dados.length;
    getAtividadesMes(); 
})};


getFeriadosAno();
var dias = ["S1D1", "S1D2", "S1D3", "S1D4", "S1D5", "S1D6", "S1D7",
"S2D1", "S2D2", "S2D3", "S2D4", "S2D5", "S2D6", "S2D7",
"S3D1", "S3D2", "S3D3", "S3D4", "S3D5", "S3D6", "S3D7",
"S4D1", "S4D2", "S4D3", "S4D4", "S4D5", "S4D6", "S4D7",
"S5D1", "S5D2", "S5D3", "S5D4", "S5D5", "S5D6", "S5D7"];

function atividadesDia (){
    dia = dias.indexOf(this.id) + 1;
    if (dia < 10) {
        dia = "0" + dia;
    }
    atividadesDia = ano + "-" + mes + "-" + dia;
    console.log("Dia Clicado " + atividadesDia);
    localStorage.setItem("wrAtividadesDia", atividadesDia);
    window.open("atividadesDia.html","_self");
}; 


function getAtividadesMes(){

    fetch("http://206.189.165.241:8080/consultaAtividadesMes/20180401", {
        method: "GET",
        headers: {
            'Content-type': 'application/json',
            'Authorization': codeAutorization
        }
    }).then(resposta => {
        if(resposta.status !== 200){
            let resp = document.getElementById("atividadesDia");
            resp.innerHTML = "Erro ao consultar atividades do Mês";
            return;
        } else{
            return resposta.json();
        }
    }).then(dados => {
        atividades = dados;
        console.log(dados);
        montaCalendario();
    });
};

function verificaHorasLancadas(dia){
    let qtdeAtividades = atividades.length;
    let totMinutos = 0;
    for (let i=0; i < qtdeAtividades; i++){
        let dataDia = atividades[i].data;
        dataDia = dataDia.toString();
        dataDia = dataDia.substr(6,2);
        if (parseInt(dataDia) == parseInt(dia)){
            console.log ("Achei Dia " + dataDia);
            let horaCalculada = atividades[i].horaCalculada; 
            let horas = horaCalculada.substr(0,2);
            let minutos = atividades[i].horaCalculada.substr(3,2);
            totMinutos += (parseInt(horas) * 60) + parseInt(minutos);
        }
    }
    console.log ("tot minutos" + totMinutos);
    if (totMinutos > 0){
        let retHoras = parseInt(totMinutos / 60);
        let retMinutos = totMinutos % 60;
        if (retHoras < 10) { retHoras = "0"+retHoras; };
        if (retMinutos < 10) { retMinutos = "0"+retMinutos; };
        return "Horas Lançadas: " + retHoras + ":" + retMinutos ;
    }
    return " ";
}

function montaCalendario(){

    for (let i=0;i <= 29;i++){
        let element = document.getElementById(dias[i]);
        element.onclick = atividadesDia;
        if(i<9){
            element.innerHTML = "0" + String(i+1);
        }
        else{
            element.innerHTML = i+1;
        }    
        
        let dataFeriado = element.innerHTML + '/' + mes + '/' + ano;
        
        for(feriado of feriadosAno){
            //console.log(feriado);
            if (feriado.date == dataFeriado & feriado.type == 'Feriado Nacional'){
                //console.log("feriado");
                element.innerHTML = element.innerHTML + " <br> " + feriado.name;
                element.style.color = "red";     
            }
        }
        let dia = i+1;
        let horasLancadas = verificaHorasLancadas(dia);
        element.innerHTML = element.innerHTML + " <br> " + " <br> " + horasLancadas ;
        element.style.font = "bold 15px arial"; 
        
    }
    //console.log(qtdeFeriadosAno);
}