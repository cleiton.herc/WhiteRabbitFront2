//LEITURA DOS CAMPOS
//------------------
let cadAtivDataDia = localStorage.getItem("wrAtividadesDia");
let codeAutorization = localStorage.getItem("wrCodeAutorization");

document.getElementById("atividadeDia").innerHTML = cadAtivDataDia;

cadAtivDataDia =  cadAtivDataDia.replace("-", "");
cadAtivDataDia =  cadAtivDataDia.replace("-", "");

let cadAtivNome = document.querySelector("#cadAtivNome");
let cadAtivDetalhe = document.querySelector("#cadAtivDetalhe");
let cadAtivCdProj = document.querySelector("#cadAtivCdProj");
let cadAtivHrIni = document.querySelector("#cadAtivHrIni");
let cadAtivHrFim = document.querySelector("#cadAtivHrFim");
let cadAtivTipo;

//funçao que será executada quando for precionado o botão Cadastrar
let botaoNovaAtividade = document.querySelector("#btnCadastrarAtividade");
botaoNovaAtividade.onclick = cadastrarAtividade;

//funçao que será executada quando for precionado o botão Cadastrar
let botaoCancelar = document.querySelector("#btnCancelarAtividade");
botaoCancelar.onclick = function(){
    window.open("atividadesDia.html","_self");
}

function cadastrarAtividade (){
    
    let checkTipo = document.getElementById("projeto");

    if (checkTipo.checked == true) {
       cadAtivTipo = "Projeto";
    } else{
       cadAtivTipo = "Administrativa";
    };

    let dados = {
        nomeAtividade: cadAtivNome.value,
        detalheAtividade: cadAtivDetalhe.value,
        tipoAtividade: cadAtivTipo,
        codigoProjeto: cadAtivCdProj.value,
        data: cadAtivDataDia,
        horaInicio: cadAtivHrIni.value,
        horaFim: cadAtivHrFim.value
    };

    console.log(dados);

    fetch("http://206.189.165.241:8080/cadastrarAtividade", {
        method: "POST",
        body: JSON.stringify(dados),
        headers: {
            'Content-type': 'application/json', 
            'Authorization': codeAutorization
        }
    }).then(resposta => {
        if(resposta.status == 403){
            //
        }

        return resposta.json();

        if (resposta.status == 200){
            window.open("atividadesDia.html","_self");
        }
    }).then(dados => {
        console.log(dados);

    });
}





